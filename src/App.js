import React from 'react';
import './App.css';
import AppHeader from "./components/app-header/app-header";
import SearchPanel from "./components/search-panel/search-panel";
import TodoList from "./components/todo-list/todo-list";
import ItemStatusFilter from "./components/item-status-filter/item-status-filter";
import AddItem from "./components/add-item/add-item";
import Calendar from "./components/calendar/calendar";

function App() {

    return (
        <div className="App">
            {/*<AppHeader toDo="1" done="7"/>*/}
            <Calendar/>
            {/*<SearchPanel/>*/}
            {/*<ItemStatusFilter/>*/}
            <TodoList/>
            <AddItem/>
        </div>
    );
}

export default App;
