import React  from "react";
import {addTask, toggleModal} from "../../redux/task-reduser";
import {connect} from "react-redux";
import FormAddItem from "./modal/form-add-item";
import {toggleModalAddTask} from "../../redux/modal-reducer";
import './add-item.css'


const AddItem = ({addTask, isModal, toggleModalAddTask}) => {

    const onToggle = () => {toggleModalAddTask(true)}

    return (
        <>
            <div className="block-add">
                <button
                    onClick={onToggle}
                    className="btn btn-outline-secondary">
                    <i className="fas fa-plus"/>
                </button>
            </div>
            {isModal &&  <FormAddItem
                toggleModal={toggleModalAddTask}
                addTask={addTask}
            />}
        </>
    )
}

let mapStateToProps = ({ modal }) => {
    return {
        isModal: modal.isModalAddTask
    }
}

export default connect(mapStateToProps, {addTask, toggleModalAddTask})(AddItem);