import React, {useEffect, useState} from "react";
import './form-add-item.css'
import {connect} from "react-redux";
import {addTask} from "../../../redux/task-reduser";

const FormAddItem = ({toggleModal, setTask}) => {

    const MONTHS = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
    const DAYS = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    const DAYS_LEAP = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    const today = new Date();
    const [date, setDate] = useState(today);
    const [day, setDay] = useState(date.getDate());
    const [month, setMonth] = useState(date.getMonth());
    const [year, setYear] = useState(date.getFullYear());
    const [startDay, setStartDay] = useState(getStartDayOfMonth(date));

    const [title, setTitle] = useState('');
    const [desc, setDesc] = useState('');
    const [color, setColor] = useState('');
    const [taskDate, setTaskDate] = useState(day);
    const [taskMonth, setTaskMonth] = useState(month);
    const [taskYear, setTaskYear] = useState(year);
    const [isEnable, setIsEnable] = useState(true);

    function getStartDayOfMonth(date) {
        return new Date(date.getFullYear(), date.getMonth(), 1).getDay();
    }

    useEffect(() => {
        setDay(date.getDate());
        setMonth(date.getMonth());
        setYear(date.getFullYear());
    }, [date]);

    // GENERATE ID
    function randomInteger(min, max) {
        // случайное число от min до (max+1)
        let rand = min + Math.random() * (max + 1 - min);
        return Math.floor(rand);
    }

    let maxId = randomInteger(5, 50);

    //SHOW TOGGLE
    const hide = () => {
        toggleModal(false);
        // console.log('sdmsd');
    }

    function isLeapYear(year) {
        return (year % 4 === 0 && year % 100 !== 0) || (year % 400 === 0);
    }

    const days = isLeapYear ? DAYS_LEAP : DAYS;

    // const isToday = today.getDate();

    const selectDays = Array(days[month] + (startDay - 1)).fill(null).map((_, index) => {
        const d = index - (startDay - 2);
        return (
            <option
                key={index}
                // defaultValue={d === isToday && isToday}
                defaultValue={taskDate}
                value={d}
            >
                {d > 0 ? d : ''}
            </option>
        )
    });

    //SUBMIT DATA

    const handleSubmit = (e) => {
        e.preventDefault();
        const newArray = {
            id: maxId,
            topic: title,
            desc: desc,
            date: taskDate,
            month: taskMonth,
            done: false,
            color: color
        }
        setTask(newArray);
        setTitle('');
        setDesc('');
        console.log(newArray);
    }

        useEffect(() => {
            setIsEnable(title.length && desc.length && taskDate.length && color.length ? false : true)
        }, [title, desc, taskDate, color])


    return (
        <div>
            <div className="modal-overlay"/>
            <div className="modal-wrapper" aria-modal aria-hidden tabIndex={-1} role="dialog">
                <div className="modal-view">
                    <div className="modal-header">
                        <button type="button"
                                className="modal-close-button"
                                data-dismiss="modal"
                                aria-label="Close"
                                onClick={hide}
                        >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form className="add-item-form" onSubmit={handleSubmit}>
                        <div className="topic">
                            <label>
                                Topic: <br/>
                                <input type="text"
                                       name="topic"
                                       onChange={(e) => setTitle(e.target.value)}
                                       value={title}
                                       placeholder="Write Topic"
                                />
                            </label>
                        </div>
                        <div className="description">
                            <label>
                                Description: <br/>
                                <input type="text"
                                       name="description"
                                       onChange={(e) => setDesc(e.target.value)}
                                       value={desc}
                                       placeholder="Write Description"
                                />
                            </label>
                        </div>
                        <div className="date">
                            <select name="day" onChange={(e) => setTaskDate(e.target.value)}>
                                {selectDays}
                            </select>
                            <select name="month" onChange={(e) => setTaskMonth(e.target.value)}>
                                {
                                    MONTHS.map((item) => {
                                        return (
                                            <option
                                                key={item}
                                                value={item}
                                                defaultValue={taskMonth}
                                            >{item}
                                            </option>
                                        )
                                    })
                                }
                            </select>
                            <select name="year" onChange={(e) => setTaskYear(e.target.value)}>
                                <option value={year} defaultValue={taskYear}>{year}</option>
                            </select>
                        </div>

                        <div className="block-circle">
                            <span className="color-info">Choose color?</span>
                            <div className="block-circle_items">
                                <input
                                    id="rad1"
                                    type="radio"
                                    name="radios"
                                    value="orange"
                                    onChange={(e) => setColor(e.target.value)}
                                />
                                <label htmlFor="rad1" className="wrap">
                                    <div className="circle orange"></div>
                                </label>

                                <input
                                    id="rad2"
                                    type="radio"
                                    name="radios"
                                    value="blue"
                                    onChange={(e) => setColor(e.target.value)}
                                />
                                <label htmlFor="rad2" className="wrap">
                                    <div className="circle blue"></div>
                                </label>

                                <input
                                    id="rad3"
                                    type="radio"
                                    name="radios"
                                    value="mint"
                                    onChange={(e) => setColor(e.target.value)}
                                />
                                <label htmlFor="rad3" className="wrap">
                                    <div className="circle mint"></div>
                                </label>
                            </div>
                        </div>
                        <button
                            type="submit"
                            disabled={isEnable}
                                className="form-btn"
                            // onClick={hide}
                            style={{background: !isEnable ? '' : '#ccc'}}
                        >ADD</button>
                    </form>
                </div>
            </div>
            <div/>
        </div>
    )
}

const mapDispatchToProps = dispatch => ({
    setTask: task => {
        dispatch(addTask(task))
    }
})


export default connect(null, mapDispatchToProps)(FormAddItem);