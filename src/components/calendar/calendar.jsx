import React, {useEffect, useState} from "react";
import "./calendar.css";
import {connect} from "react-redux";
import Circle from "./circle/circle";
import {toggleModalMain} from "../../redux/modal-reducer";
import {selectDate} from "../../redux/select-date-reducer";


const Calendar = ({ todoData, toggleModalMain, selectDate }) => {

    console.log(todoData);

    const DAYS = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    const DAYS_LEAP = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    const DAYS_OF_THE_WEEK = ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'];
    const MONTHS = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];

    const today = new Date();
    const [date, setDate] = useState(today);
    const [day, setDay] = useState(date.getDate());
    const [month, setMonth] = useState(date.getMonth());
    const [year, setYear] = useState(date.getFullYear());
    const [startDay, setStartDay] = useState(getStartDayOfMonth(date));

    useEffect(() => {
        setDay(date.getDate());
        setMonth(date.getMonth());
        setYear(date.getFullYear());
        setStartDay(getStartDayOfMonth(date));
    }, [date]);

    function getStartDayOfMonth(date) {
        return new Date(date.getFullYear(), date.getMonth(), 1).getDay();
    }

    function isLeapYear(year) {
        return (year % 4 === 0 && year % 100 !== 0) || (year % 400 === 0);
    }

    const days = isLeapYear ? DAYS_LEAP : DAYS;

    const isToday = today.getMonth();

    const handleClick = (d) => {
       setDate(new Date(year, month, d));
        toggleModalMain(false);
        selectDate(d)
    }
    return (
        <div className="frame">
            <div className="header">
                {/*<button className="btn-c" onClick={() => setDate(new Date(year, month - 1, day))}>Prev</button>*/}
                    <h5>{MONTHS[month]} {year}</h5>
                {/*<button className="btn-c" onClick={() => setDate(new Date(year, month + 1, day))}>Next</button>*/}
            </div>
            <div className="body-c">
                {DAYS_OF_THE_WEEK.map((item) => {
                    return (<div className="week-c" key={item}>{item}</div>)
                })
                }
                    {
                        Array(days[month] + (startDay - 1)).fill(null).map((_, index) => {
                        const d = index - (startDay - 2);
                        return (
                                <div
                                    key={index}
                                    className={d === day ? "day-c today selected" : "day-c"}
                                    onClick={() => handleClick(d)}
                                >
                                    <p>{d > 0 ? d : ''}</p>
                                    <Circle
                                        todoData={todoData} day={d} month={MONTHS[month]}
                                    />

                                </div>
                        )

                    })}
            </div>

        </div>
    );
}

let mapStateToProps = (state) => ({
        todoData: state.todoList.todoData
    })

export default connect(mapStateToProps, {toggleModalMain, selectDate})(Calendar);