import React from "react";
import './circle.css'


const Circle = ({todoData, day, month}) => {

    return (
        <>
            {
                todoData.map(({id, color, date}) => {
                    const d = date === day;
                    return (
                        <div key={id}
                             className={d ? `circle-c top-c ${color}` : ''}>
                        </div>
                    )
                })
            }
        </>
    )
}

export default Circle;
