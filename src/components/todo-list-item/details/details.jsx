import React from "react";
import './details.css';


const Details = (props) => {

    const deleteItem = (id) => {
        props.deleteTask(id);
        props.showDetail(false);
    }
    return (
        <div className="modal-overlay" onClick={() => props.showDetail(false)}>
            <div className="modal-wrapper">
                <div className="modal-view-detail">
                    {
                        props.data.filter(el => el.id === props.selectedTask).map(({id, topic, desc, date, month, color, done}) => {
                                return (
                                    <>
                                        <div className="block-title">
                                            <div className={`circle-c ${color}`}></div>
                                            <h2 className={done ? 'detail-title left-pad through' : 'detail-title left-pad' }>{topic}</h2>
                                        </div>
                                        <div className="block-datetime">
                                            <i class="far fa-calendar-alt"/>
                                            <span>{date}</span>
                                            <span>{month}</span>
                                        </div>
                                        <div className="block-desc">
                                            <p>{desc}</p>
                                        </div>
                                        <div className="">
                                            <i className={!done ? "fas fa-check unfinish" : "fas fa-check finish"}  />
                                            <span>{!done ? 'uncompleted' : 'complete'}</span>
                                        </div>
                                        <div className="block-btn-delete">
                                            <button className="btn-del" onClick={() => deleteItem(id)}>Delete</button>
                                        </div>
                                    </>
                                )
                            })
                    }
                </div>
            </div>
        </div>
    )
}

export default Details;