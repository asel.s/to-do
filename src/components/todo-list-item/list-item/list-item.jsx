import React from "react";
import {deleteTask, setDone, setTaskId} from "../../../redux/task-reduser";
import "./list-item.css";
import { connect } from "react-redux";
import { toggleModalDetails} from "../../../redux/modal-reducer";
import Details from "../details/details";

const ListItem = (props) => {

    const { id, done, topic, desc, date, month, color} = props.tasks;

    const deleteItem = id => {
        props.deleteTask(id);
    };

    const onComplete = id => {
        props.setDone(id);
    };

    const showDetail = (id) => {
        props.setTaskId(id);
        props.toggleModalDetails(true)
    };

    return (
        <li className="list-group-item">
            <div className="todo-list-item-label">
                <i
                    className={!done ? "fas fa-check unfinish" : "fas fa-check finish"}
                    onClick={() => onComplete(id)}
                />
                <span className="title">{topic}</span>
                <span className="dateTask">
                    {date}
                    {month}
                </span>
                <div className="btn-block">
                    <span onClick={() => showDetail(id)}>View details</span>
                    <i className="fas fa-trash" onClick={() => deleteItem(id)} />
                </div>
                {props.modalDetails && <Details show={ props.modalDetails }
                                                selectedTask={ props.selectedTask }
                                                data = { props.todoData }
                                                deleteTask = { props.deleteTask }
                                                showDetail = { props.toggleModalDetails }
                />}
            </div>
        </li>
    );
};

let mapStateToProps = state => ({
        modalDetails: state.modal.isModalDetails,
        selectedTask: state.todoList.taskID,
        todoData: state.todoList.todoData,
});

export default connect(
    mapStateToProps,
    { deleteTask, setDone, toggleModalDetails, setTaskId }
)(ListItem);
