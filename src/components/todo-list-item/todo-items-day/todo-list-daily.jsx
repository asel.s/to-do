import React from "react";
import {connect} from "react-redux";
import ListItem from "../list-item/list-item";


const TodoListDaily = ({selectedDate, todoData}) => {
    console.log(todoData);
    return (
        <div className="todo-list-wrap">
            <h1 className="title-card">{selectedDate + ` th`}</h1>
            <ul className="list-group todo-list">
                {todoData
                    .filter(item => item.date === selectedDate)
                    .map(item => {
                        return <ListItem key={item.id}
                            tasks = {item}
                        />;
                })}
            </ul>
        </div>
    )
}

let mapStateToProps = (state) => ({
        todoData: state.todoList.todoData,
        selectedDate: state.selectDate.selectDate,
})


export default connect(mapStateToProps)(TodoListDaily);

