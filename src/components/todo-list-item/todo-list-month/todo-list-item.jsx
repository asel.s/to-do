import React, { useState} from "react";
import './todo-list-month.css';
import Details from "./details/details";
import {connect} from "react-redux";
import {toggleModalDetails} from "../../redux/modal-reducer";
import {setDone} from "../../redux/task-reduser";
import ToDoDetail from "./todo-items-day/todo-items-day";

const elements = todoData.map((item) => {

    const { id, ...itemProps } = item;

    const deleteItem = (id) => {
        deleteTask(id);
    }

    const completeTask = (id) => {
        setDone(id)
    }

    return (
        <li key={id} className="list-group-item">
            {isModalMain ? <TodoListItem
                {...item}
                onDeleted={() => deleteItem(id)}
                addTask={addTask}
                onComplete={() => completeTask(id)}
            /> : <ToDoDetail date={selectedDate} todoData={exist}/>}
        </li>
    );
})

const TodoListMonth = ({elements}) => {
   return ( <div className="todo-list-wrap">
        <h1 className="title-card">This month</h1>
        <ul className="list-group todo-list">
            {elements}
        </ul>
    </div>
   )
}

export default TodoListMonth;

const TodoListItem = ({ id, topic, done, date, month, onDeleted, isModal, toggleModalDetails, onComplete, isModalMain}) => {
    const [important, setImportant] = useState(false);

    // if (done) {
    //     classNames += ' done'
    // }

   return (
       <div className="wrap-list-item">
               <div className="todo-list-item-label">
                   <i className={!done ? "fas fa-check unfinish" : "fas fa-check finish"} onClick={onComplete} />
                   <span className="title">
                      {topic}
                   </span>
                   <span className="dateTask">
                       {date}
                       {month}
                   </span>
               </div>
               {/*<p onClick={() => toggleModalDetails(true)}>View details</p>*/}
           {/*<div className="icons-block">*/}
           {/*    <i className="fas fa-trash"  onClick={onDeleted}/>*/}
           {/*</div>*/}
           {/*{isModal && <Details />}*/}
       </div>
      )

}

let mapStateToProps = ({ modal }) => {
    return {
        isModal: modal.isModalDetails,

    }
}

// export default connect(mapStateToProps, {toggleModalDetails})(TodoListItem);