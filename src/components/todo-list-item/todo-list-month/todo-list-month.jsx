import React, { useState} from "react";
import './todo-list-month.css';
import {connect} from "react-redux";
import {addTask, deleteTask, setDone} from "../../../redux/task-reduser";

const TodoListItem = ({ topic, done, date, month, onComplete}) => {

    return (
        <div className="wrap-list-item">
            <div className="todo-list-item-label">
                <i className={!done ? "fas fa-check unfinish" : "fas fa-check finish"} onClick={onComplete} />
                <span className="title">
                      {topic}
                   </span>
                <span className="dateTask">
                       {date}
                    {month}
                </span>
            </div>
        </div>
    )
}

const TodoListMonth = ({ todoData, deleteTask,  addTask, setDone}) => {

    const deleteItem = (id) => {
        deleteTask(id);
    }

    const completeTask = (id) => {
        setDone(id)
    }
    return (
        <div className="todo-list-wrap">
            <h1 className="title-card">This month</h1>
            <ul className="list-group todo-list">
                {
                    todoData.map((item) => {

                    const { id, ...itemProps } = item;
                    return (
                        <li key={id} className="list-group-item">
                            <TodoListItem
                                {...item}
                                onDeleted={() => deleteItem(id)}
                                addTask={addTask}
                                onComplete={() => completeTask(id)}
                            />
                        </li>
                    );
                })
                }
            </ul>
        </div>
    )
}

let mapStateToProps = (state) => ({
        todoData: state.todoList.todoData,
        isModal: state.modal.isModalDetails,
});


export default connect(mapStateToProps, {deleteTask,  addTask, setDone})(TodoListMonth);

