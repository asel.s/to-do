import React from "react";
import './todo-list.css';
import {connect} from "react-redux";
import TodoListDaily from "../todo-list-item/todo-items-day/todo-list-daily";
import TodoListMonth from "../todo-list-item/todo-list-month/todo-list-month";


const TodoList = ({ isModalMain }) => {

    return (
        <>
            {isModalMain
                ? <TodoListMonth/>
                : <TodoListDaily/>
            }
        </>
    );
}

let mapStateToProps = (state) => ({
        todoData: state.todoList.todoData,
        isModalMain: state.modal.isModalMain,
        selectedDate: state.selectDate.selectDate
})


export default connect(mapStateToProps)(TodoList);

