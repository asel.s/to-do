const TOGGLE_ADD_TASK_MODAL = "TOGGLE_ADD_TASK_MODAL";
const TOGGLE_DETAILS_MODAL = "TOGGLE_DETAILS_MODAL";
const TOGGLE_MAIN_MODAL = "TOGGLE_MAIN_MODAL";

let initialState = {
      isModalAddTask: false,
      isModalDetails: false,
      isModalMain: true,
};
const modalReduser = (state = initialState, action) => {
    switch(action.type) {

        case TOGGLE_ADD_TASK_MODAL:
            return {
                ...state,
                isModalAddTask: action.a
            }
        case TOGGLE_DETAILS_MODAL:
            return {
                ...state,
                isModalDetails: action.d
            }
        case TOGGLE_MAIN_MODAL:
            return {
                ...state,
                isModalMain: action.m
            }
        default:
            return state;
    }
}

export const toggleModalAddTask = (a) => ({type: TOGGLE_ADD_TASK_MODAL, a});
export const toggleModalDetails = (d) => ({type: TOGGLE_DETAILS_MODAL, d});
export const toggleModalMain = (m) => ({type: TOGGLE_MAIN_MODAL, m});


export default modalReduser;