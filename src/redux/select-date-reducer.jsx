const SELECT_DATE = 'SELECT_DATE';

let initialState = {
    selectDate: null
}

const selectDateReducer = (state=initialState, action) => {
    switch(action.type) {
        case SELECT_DATE:
            return {
                selectDate: action.num
            }
        default:
            return state;
    }
}

export const selectDate = (num) => ({type: 'SELECT_DATE', num});

export default selectDateReducer;