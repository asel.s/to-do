import {combineReducers, createStore} from "redux";
import taskReduser from "./task-reduser";
import modalReduser from "./modal-reducer";
import selectDateReducer from "./select-date-reducer";

let reducers = combineReducers ({
    todoList: taskReduser,
    modal: modalReduser,
    selectDate: selectDateReducer
})

let store = createStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

window.store = store;

export default store;