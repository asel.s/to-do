const SET_TODO_ITEM = "SET_TODO_ITEM";
const DELETE_ITEM = "DELETE_ITEM";
const ADD_ITEM = "ADD_ITEM";
const DONE_TASK = "DONE_TASK";
const SET_TASK_ID = "SET_TASK_ID";


let initialState = {
    todoData: [
        { id: 1, topic: 'Reading', desc: 'Develop site and doing code review!', date: 2 , month: 'MAY', done: true, color: 'orange'},
        { id: 2, topic: 'Cleaning', desc: 'Make Awesome App', date: 10, month: 'MAY', done: false, color: 'blue' },
        { id: 3, topic: 'Meeting', desc: 'Have a lunch', date: 2, month: 'MAY', done: false, color: 'mint' },
        { id: 4, topic: 'Play chess', desc: 'Have a lunch', date: 8, month: 'MAY', done: false, color: 'orange' },
    ],
    term: '',
    taskID: null
};

const taskReduser = (state = initialState, action) => {

    switch(action.type) {
        case SET_TODO_ITEM:
            return {
                ...state,
                todoData: action.item
            }
        case DELETE_ITEM:

            // const idx = state.todoData.findIndex((el) => el.id === action.id);
            // const newArr = [
            //     ...state.todoData.slice(0, idx),
            //     ...state.todoData.slice(idx + 1)
            // ];

            return {
                ...state,
                todoData: state.todoData.filter((el) => el.id !== action.id)
                // todoData: newArr
            }
        case ADD_ITEM:
            return {
                ...state,
                todoData: [...state.todoData, action.task],
            }
        case DONE_TASK:
            return {
                ...state,
                todoData: state.todoData.map(item => item.id === action.id ? {...item, done: !item.done} : item)
            }
        case SET_TASK_ID:
            return {
                ...state,
                taskID: action.taskId
            }
        default:
            return state;
    }
}

export default taskReduser;

export const todoItem = (item) => ({type: SET_TODO_ITEM, item});
export const deleteTask = (id) => ({type: DELETE_ITEM, id});
export const addTask = (task) => ({type: ADD_ITEM, task});
export const setDone = (id) => ({type: DONE_TASK, id});
export const setTaskId = (taskId) => ({type: SET_TASK_ID, taskId})

